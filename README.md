# Leaf area index at the EucFACE

This repository contains the code to process raw data from HIEv and estimate daily canopy gap fraction and leaf area index (LAI) at the EucFACE. To be able to execute the code yourself, you need to be affiliated with the Hawkesbury Institute for the Environment, and have an active account on HIEv. 

This code is executed daily on a computer at EucFACE, and results uploaded to HIEv (final output: FACE_P0037_RA_GAPFRACLAI_OPEN_L2.dat; intermediate 30min PAR data: FACE_P0037_RA_PARAGG_YYYY-MM-DD_YYYY-MM-DD.csv).

Methods of data processing, subsetting, and estimation of LAI follow [Duursma et al. 2015 (Glob. Change Biol)](http://onlinelibrary.wiley.com/doi/10.1111/gcb.13151/full) (see also the [code to generate the publication](https://www.github.com/eucfacelaipaper)).

## Instructions

- Make sure you have installed the [HIEv R package](https://www.bitbucket.org/remkoduursma/hiev).
- In R, execute `source("run.R")`. This will take at least several minutes the first time, but will be much faster on subsequent runs. 
- **Note** :  local cache of ca. 2GB will be created on first run (in the 'cache' subdirectory of the repository).