



tdata <- subset(airt, year(Date) == 2012)

nd <- nrow(tdata)
dalph <- 2*pi / nd

rad <- seq(dalph, 2*pi, length=nd)

seg <- function(alph, tmin, tmax, zerocircle=10, ...){
  segments(x0=(tmax+zerocircle)*sin(alph), 
           x1=(tmin+zerocircle)*sin(alph), 
           y0=(tmax+zerocircle)*cos(alph), 
           y1=(tmin+zerocircle)*cos(alph), ...)
}

cuttair <- cut(tdata$Tair, 101)
cr <- colorRampPalette(c("blue","red"))
cols <- cr(nlevels(cuttair))[cuttair]

windows()
par(pty='s')
plot(1, type='n', xlim=c(-50,50), ylim=c(-50,50), axes=F, ann=F)
symbols(x=rep(0,4), y=rep(0,4), circles=seq(10,40,by=10), fg="darkgrey", add=TRUE, inches=FALSE)
for(i in 1:nd)seg(rad[i], tdata$Tmin[i], tdata$Tmax[i], col=cols[i], lwd=2)



