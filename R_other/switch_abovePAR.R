
facepar1 <- subset(facepar, Ring =="R1")
facepar3 <- subset(facepar, Ring =="R3")

facepar1x <- merge(facepar1, facepar3[,c("DateTime","LI190SB_PAR_Den_Avg")], by="DateTime")
facepar3x <- merge(facepar3, facepar1[,c("DateTime","LI190SB_PAR_Den_Avg")], by="DateTime")


getdaydf <- function(df){
  
  df$PAR_Den <- rowMeans(df[,c("PAR_Den_1_Avg","PAR_Den_2_Avg","PAR_Den_3_Avg")],
                         na.rm=TRUE)
  df$Gapfraction_self <- with(df, PAR_Den / LI190SB_PAR_Den_Avg.x)
  df$Gapfraction_other <- with(df, PAR_Den / LI190SB_PAR_Den_Avg.y)
  
  df$Fdiff <- with(df, DiffuseSS / TotalSS)
  df <- subset(df, Fdiff > 0.98)
  df <- subset(df, LI190SB_PAR_Den_Avg.x > 250 & LI190SB_PAR_Den_Avg.x < 1300)
  
  
  n <- function(x,...)length(x[!is.na(x)])
  dfa <- summaryBy(Gapfraction_self + Gapfraction_other ~ Date, data=df, FUN=c(mean,sd,n), na.rm=TRUE)
  dfa <- subset(dfa, Gapfraction_self.n > 3)
  # cupgap <- subset(cupgap, Gapfraction1.mean > 0.05)  # one massive outlier

  
return(dfa)
}

gap1 <- getdaydf(facepar1x)
gap3 <- getdaydf(facepar3x)

windows(8,4)
par(mfrow=c(1,2), mar=c(5,5,1,1), cex.lab=1.1)
with(gap1, plot(Gapfraction_self.mean, Gapfraction_other.mean,
                xlab=expression(tau~(above~below~Ring1)),
                ylab=expression(tau~(below~Ring1~above~Ring3))
                ))
abline(0,1)
with(gap3, plot(Gapfraction_self.mean, Gapfraction_other.mean,
                xlab=expression(tau~(above~below~Ring3)),
                ylab=expression(tau~(below~Ring3~above~Ring1))
))
abline(0,1)

windows(10,8)
par(mfrow=c(2,1), mar=c(5,5,1,1))
with(gap1, plot(Date, Gapfraction_other.mean, pch=21, bg="white", ylim=c(0,0.4),
                ylab="Gap fraction (-)"))
with(gap1, points(Date, Gapfraction_self.mean, pch=16, cex=0.8))
legend("bottomleft", c("Own above PAR","Ring 3 above PAR"), title="Ring 1", pch=c(16,1))  

with(gap3, plot(Date, Gapfraction_other.mean, pch=21, bg="white", ylim=c(0,0.4),
                ylab="Gap fraction (-)"))
with(gap3, points(Date, Gapfraction_self.mean, pch=16, cex=0.8))
legend("bottomleft", c("Own above PAR","Ring 1 above PAR"), title="Ring 3", pch=c(16,1))  
dev.copy2pdf(file="output/figures_other/gapfrac_face_switchabove.pdf")



sm1_self <- smoothplot(Date, Gapfraction_self.mean, kgam=18, data=gap1)
sd(residuals(sm1_self[[1]]))

sm1_other <- smoothplot(Date, Gapfraction_other.mean, kgam=18, data=gap1)
sd(residuals(sm1_other[[1]]))

